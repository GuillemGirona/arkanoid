﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
    public Vector2 speed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(speed*Time.deltaTime);
	}

    private void OnTriggerEnter(Collider other)
    {
        switch(other.tag)
        {
            case "Right":
            case "Left":
                speed.x = -speed.x;
                break;
            case "Down":
            case "Top":
                speed.y = -speed.y;
                break;
            case "Player":
                speed.y = -speed.y;
                break;
            case "Brick":
                other.gameObject.GetComponent<Brick>().Touch();
                speed.y = -speed.y;
                break;
        }
    }
}
